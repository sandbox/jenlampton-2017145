<?php

/**
 * @ file
 * Integration with Vantage Media API.
 */

/**
 * Implementation of hook_menu().
 */
function vantage_menu() {
  $items = array();
  $items['admin/settings/api/vantage'] = array(
    'title' => 'Vantage Settings',
    'description' => 'Vantage settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('vantage_settings_form'),
    'access arguments' => array('administer vantage'),
    'file' => 'vantage.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/settings/api/vantage/test'] = array(
    'title' => 'Find schools near you',
    'page callback' => 'vantage_listing_page',
    'access arguments' => array('administer vantage'),
    'type' => MENU_CALLBACK,
  );
  $items['admin/settings/api/vantage/test/%/%'] = array(
    'title' => 'Find schools near you',
    'page callback' => 'vantage_listing_page',
    'page arguments' => array(1, 2),
    'access arguments' => array('administer vantage'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implentation of hook_perm().
 */
function vantage_perm() {
  return array('administer vantage');
}

/**
 * Callback that generates school listings.
 *
 * @param $state - 2 letter abbreviation for state 
 * @param $zip - 5 digit zip code.
 */
function vantage_listing_page($state = NULL, $zip = NULL) {
  // Check the incoming args for 'all' and unset.
  $state = ($state != 'all') ? $state : 'CA';
  $zip = ($zip != 'all') ? $zip : NULL;

  // Add a stylesheet to the page.
  drupal_add_css(drupal_get_path('module', 'vantage') . '/vantage.css');

  $settings = array(
    'id' => 'schools',
    'campus' => '3', // Both.
  );

  if ($state != NULL || $zip != NULL) {
    // Show state schools at the top of the page, if state or zip is given.
    if ($state != NULL) {
      $settings['state'] = $state;
    }
    if ($zip != NULL) {
      $settings['zip'] = $zip;
    }
  }
  $output  = '<div class="campus-schools schools">';
  $output .= '<h4>' . t('Schools Matching Your Search') . '</h4>';
  $output .= vantage_build_listing($settings);
  $output .= '</div>';

  return $output;
}

/**
 * Form that allows a user to narrow down school results.
 *
 * @param $form_state - submitted values.
 * @param $widget - If this form is being showed in a widget or not.
 */
function vantage_refine_form($form_state, $widget = FALSE) {
  // See if the user used the filter form and if so set defaults.
  if (!$widget) {
    $state = (arg(1) != 'all')? arg(1) : 'CA';
    //$zip = (arg(2) != 'all')? arg(2) : NULL;
  }

  $form = array();

  $states = vantage_state_list();
  $form['state'] = array(
    '#title' => t('Location'),
    '#type' => 'select',
    '#options' => $states,
    '#default_value' => isset($state) ? $state : 'CA',
    '#required' => TRUE,
  );

/*
  $form['zip'] = array(
    '#type' => 'textfield',
    '#title' => t('Zip Code'),
    '#size' => 7,
    '#maxlength' => 5,
    '#default_value' => isset($zip) ? $zip : '',
  );
*/

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Find Schools'),
  );

  if ($widget === TRUE) {
    // Remove the title on the state drop-down.
    unset($form['state']['#title']);
  }

  return $form;
}

/**
 * Submit handler for vantage refine form.
 */
function vantage_refine_form_submit($form, &$form_state) {
  $path = 'find-a-school/';

  // First, check for state and append to url.
  if ($form_state['values']['state'] != '') {
    $path .= $form_state['values']['state'] . '/';
  }
  else {
    $path .= 'all/';
  }

/*
  // Then, check for zip.
  if ($form_state['values']['zip'] != '') {
    $path .= $form_state['values']['zip'] . '/';
  }
  else {
    $path .= 'all/';
  }
*/

  // Remove the final slash.
  $path = substr($path, 0, -1);

  // Redirect to the new page.
  drupal_goto($path);
}

/**
 * Implementation of hook_ctools_plugin_api().
 */
function vantage_ctools_plugin_api($module, $api) {
  if ($module == 'page_manager' && $api == 'pages_default') {
   return array('version' => 1);
  }
  if ($module == 'panels_mini' && $api == 'panels_default') {
    return array('version' => 1);
  }  
}

/**
 * Implementation of hook_ctools_plugin_dierctory().
 */
function vantage_ctools_plugin_directory($module, $plugin) {
  return 'plugins/' . $plugin;
}

/**
 * Implementation of hook_block().
 * Featured schools "listing" hard coded.
 */
function vantage_block($op = 'list', $delta = 0, $edit = array()) {
  if ($op == 'list') {
    $blocks['featured'] = array(
      'info' => t('Vantage featured schools'), 
    );

    return $blocks;
  }

  else if ($op == 'view') {
    switch ($delta) {
      case 'featured':

        $block = array(
          'subject' => t('Featured schools'),
          'content' => vantage_block_featured(),
        );
        break;
    }

    return $block;
  }
}

/**
 * Helper function
 *
 * @return $schools - hard-coded schools listing until API is fixed.
 */
function vantage_block_featured() {
  $ai_path = drupal_get_path('module', 'vantage') . '/images/the-art-institutes.gif';
  $ai_text = 'The Art Institutes';
  $ai_image = theme('image', $ai_path, $ai_text, $ai_text);
  $ai_link = 'http://aff.vantageoffers.com/?kid=HLB0&pub_id=2473&cam_id=L115&data1=Featured&data2=&a_aid=2357&a_bid=L115L1';
  $ai = l($ai_image, $ai_link, array('html' => TRUE, 'attributes' => array('target' => '_blank')));

  $iadt_path = drupal_get_path('module', 'vantage') . '/images/international-academy-of-design-and-technology.gif';
  $iadt_text = 'IADT Online';
  $iadt_image = theme('image', $iadt_path, $iadt_text, $iadt_text);
  $iadt_link = 'http://aff.vantageoffers.com/?kid=HEYQ&pub_id=2473&cam_id=L178&data1=Featured&data2=&a_aid=2357&a_bid=L178L1';
  $iadt = l($iadt_image, $iadt_link, array('html' => TRUE, 'attributes' => array('target' => '_blank')));

  $fs_path = drupal_get_path('module', 'vantage') . '/images/fullsailuniversity.gif';
  $fs_text = 'Full Sail';
  $fs_image = theme('image', $fs_path, $fs_text, $fs_text);
  $fs_link = 'http://aff.vantageoffers.com/?kid=1MO1L&pub_id=2473&cam_id=L310&data1=Featured&data2=&a_aid=2473&a_bid=L310L1';
  $fs = l($fs_image, $fs_link, array('html' => TRUE, 'attributes' => array('target' => '_blank')));

  $schools  = '<div class="art-institutes">';
  $schools .= $ai;
  $schools .= '</div>';

  $schools.= '<div class="iadt">';
  $schools .= $iadt;
  $schools .= '</div>';

  $schools.= '<div class="full-sail">';
  $schools .= $fs;
  $schools .= '</div>';

  return $schools;
}


/*********************
 * Helper functions.
 *********************/

/**
 * Helper function: Builds schools listing.
 *
 *  @param $settings - Array of settings needed for the javascript:
 *    array(
 *      'id' => 'featured',
 *      'state' => 'CA',
 *      'zip' => '94618',
 *      'degree' => '2', // see mapping in vantage.admin.inc.
 *      'results' => '10',
 *      'campus' => '3', // see mapping in vantage.admin.inc.
 *     );
 *
 *  @return The rendered JS tag that will produce a schools listing.
 */
function vantage_build_listing($settings = array()) {
  // If no ID, return.
  if (!array_key_exists('id', $settings)) {
    return;
  }

  // Retrieve site wide settings as needed.
  $aid = variable_get('vantage_aid', '12535');
  $cid = (array_key_exists('cid', $settings)) ? $settings['cid'] : variable_get('vantage_cid', '6434');
  $pid = variable_get('vantage_pid', '601');
  $state = (array_key_exists('state', $settings)) ? $settings['state'] : '';
  $zip = (array_key_exists('zip', $settings)) ? $settings['zip'] : '';
  $degree = (array_key_exists('degree', $settings)) ? $settings['degree'] : ''; // No default?
  $campus = (array_key_exists('vantage_campus', $settings)) ? $settings['campus_type'] : variable_get('vantage_campus', '3');
  $result_size = (array_key_exists('results', $settings)) ? $settings['results'] : variable_get('vantage_result_size', '10');

  // See if the user used the filter form and if so alter the query.
  if (array_key_exists('zip', $settings)) {
    $location = $settings['zip'];
  }
  else if (array_key_exists('state', $settings)) {
    $location = $settings['state'];
  }
  else {
    $location = variable_get('vantage_location', 'all');
  }
  if ($location != '' && $location != 'all') {
    $distance = '100'; // @TODO make this 30 to match QS?
  }

  // Add JS to page.
  $output  = '<div id="' . $settings['id'] . '-listings" class="school-listings">';
  $output .= '  <script type="text/javascript" src="//www.bwserver.net/scripts/jsgeo.php"></script>';
  $output .= '  <script type="text/javascript">
    bwapsprodid = "' . $pid . '";
    bwapsstate = "' . $state . '";
    bwapszip = "' . $zip . '";
    bwapsaid = "' . $aid . '";
    bwapscid = "' . $cid . '";
    bwapsadsource = "";
    bwapssidebar = "hide";
    educampus = "' . $campus . '";
    bwapsmaxads = "' . $result_size . '";
    edudegreetype = "' . $degree . '";
    vm_layout = "pagestack";
  </script>';
  
  switch ($settings['id']) {
    case 'schools':
      $output .= '  <script type="text/javascript" src="//cdn.bwserver.net/12500/12535/scripts/bw-load-edu-6434.js"></script>';
      break;
    case 'featured':
      $output .= '  <script type="text/javascript" src="//cdn.bwserver.net/12500/12535/scripts/bw-load-edu-6632.js"></script>';
      break;
  }

  $output .= '</div>';

  return $output;
}

/**
 * Helper function: Returns a state abbr.
 *
 *  @param $term - fully loaded taxonomy term.
 *
 *  @return array $state - QS state abbr based on term.
 */
function vantage_get_state_from_term($term) {
  $states = vantage_state_list();

  // Term name cleanup for ACR.
  $term_state = trim(str_replace('Animation Schools', '', $term->name));
  $state = array_search($term_state, $states);
  if ($state) {
    return $state;
  }
  else {
    // Special treatment for DC.
    $dc = trim(str_replace('(DC)', '', $term_state));
    $state = array_search($dc, $states);
    if ($state) {
      return $state;
    }

    // if not found, return FALSE.
    return FALSE;
  }
}

/**
 * Helper function: Returns a list of states for the widget.
 *
 *  @return array States - keyed array of states.
 */
function vantage_state_list() {
  $states = array(
    '' => t('-Select a state-'),
    'AL' => t('Alabama'),
    'AK' => t('Alaska'),
    'AZ' => t('Arizona'),
    'AR' => t('Arkansas'),
    'CA' => t('California'),
    'CO' => t('Colorado'),
    'CT' => t('Connecticut'),
    'DE' => t('Delaware'),
    'DC' => t('District of Columbia'),
    'FL' => t('Florida'),
    'GA' => t('Georgia'),
    'HI' => t('Hawaii'),
    'ID' => t('Idaho'),
    'IL' => t('Illinois'),
    'IN' => t('Indiana'),
    'IA' => t('Iowa'),
    'KS' => t('Kansas'),
    'KY' => t('Kentucky'),
    'LA' => t('Louisiana'),
    'ME' => t('Maine'),
    'MD' => t('Maryland'),
    'MA' => t('Massachusetts'),
    'MI' => t('Michigan'),
    'MN' => t('Minnesota'),
    'MS' => t('Mississippi'),
    'MO' => t('Missouri'),
    'MT' => t('Montana'),
    'NE' => t('Nebraska'),
    'NV' => t('Nevada'),
    'NH' => t('New Hampshire'),
    'NJ' => t('New Jersey'),
    'NM' => t('New Mexico'),
    'NY' => t('New York'),
    'NC' => t('North Carolina'),
    'ND' => t('North Dakota'),
    'OH' => t('Ohio'),
    'OK' => t('Oklahoma'),
    'OR' => t('Oregon'),
    'PA' => t('Pennsylvania'),
    'RI' => t('Rhode Island'),
    'SC' => t('South Carolina'),
    'SD' => t('South Dakota'),
    'TN' => t('Tennessee'),
    'TX' => t('Texas'),
    'UT' => t('Utah'),
    'VT' => t('Vermont'),
    'VA' => t('Virginia'),
    'WA' => t('Washington'),
    'WV' => t('West Virginia'),
    'WI' => t('Wisconsin'),
    'WY' => t('Wyoming'),
  );

  return $states;
}
