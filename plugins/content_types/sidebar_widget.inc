<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Vantage sidebar widget'),
  'icon' => 'vantage.gif',
  'description' => t('Vantage sidebar widget'),
  'category' => t('Widgets'),
);

function vantage_sidebar_widget_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->title = t('Find a school near you!');
  $block->content = drupal_get_form('vantage_refine_form', NULL);

  return $block;
}

function vantage_sidebar_widget_content_type_admin_title($subtype, $conf, $context) {
  return t('Vantage sidebar widget');
}
