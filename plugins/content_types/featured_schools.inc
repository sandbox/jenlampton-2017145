<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Vantage Featured Schools'),
  'icon' => 'vantage.gif',
  'description' => t('Vantage featured schools logos'),
  'category' => t('Miscellaneous'),
);

function vantage_featured_schools_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->title = t('Featured schools');

  // @todo make this use the API instead of using hard-coded schools.
  // $cid = variable_get('vantage_cid_featured', '');
  // $block->content = vantage_build_listing($settings);
  $block->content = vantage_block_featured();

  return $block;
}

function vantage_featured_schools_content_type_admin_title($subtype, $conf, $context) {
  return t('Vantage featured schools');
}
