<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Vantage Category Schools listing'),
  'icon' => 'vantage.gif',
  'description' => t('Vantage Category page schools listing'),
  'required context' => new ctools_context_required(t('Term'), 'term'),
  'category' => t('Taxonomy term'),
);

function vantage_category_schools_content_type_admin_title($subtype, $conf, $context) {
  return t('"@s" Vantage category listing', array('@s' => $context->identifier));
}

function vantage_category_schools_content_type_render($subtype, $conf, $panel_args, $context) {
  $term = isset($context->data) ? drupal_clone($context->data) : NULL;

  $cid = variable_get('vantage_cid_incontent', '');
  $settings = array(
    'id' => 'schools',
    'campus' => '3', // Both.
    'results' => '5', // Number of items in listing.
    'cid' => $cid,
  );

  $state = vantage_get_state_from_term($term);
  if ($state) {
    $settings['state'] = $state;
  }

  $block = new stdClass();
  $block->title = t('Programs to consider:');
  $block->content = vantage_build_listing($settings);

  return $block;
}

function vantage_category_schools_content_type_edit_form(&$form, &$form_state) {
  // Provide a blank form so we have a place to have context setting.
}
