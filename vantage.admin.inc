<?php

/**
 * @ file
 * Admin settings form for Vantage integration.
 */

/**
 * Admin settings FAPI form.
 */
function vantage_settings_form() {
  $form = array();

  $form['vantage_aid'] = array(
    '#type' => 'textfield',
    '#title' => t('Affiliate ID'),
    '#description' => t('This value will be provided to you by Vantage'),
    '#default_value' => variable_get('vantage_aid', ''),
    '#required' => TRUE,
  );

  $form['vantage_cid'] = array(
    '#type' => 'textfield',
    '#title' => t('Campaign - Find a School'),
    '#description' => t('Please enter the <em>Find a School</em> campaign id provided by Vantage.'),
    '#default_value' => variable_get('vantage_cid', ''),
    '#size' => 10,
  );
  $form['vantage_cid_featured'] = array(
    '#type' => 'textfield',
    '#title' => t('Campaign - Featured Schools'),
    '#description' => t('Please enter the <em>Featured Schools</em> campaign id provided by Vantage.'),
    '#default_value' => variable_get('vantage_cid_featured', ''),
    '#size' => 10,
  );
  $form['vantage_cid_incontent'] = array(
    '#type' => 'textfield',
    '#title' => t('Campaign - In-content listing'),
    '#description' => t('Please enter the <em>In-content listing</em> campaign id provided by Vantage.'),
    '#default_value' => variable_get('vantage_cid_incontent', ''),
    '#size' => 10,
  );

  $options_pid = array(
    '600' => 'Business',
    '601' => 'Creative Arts & Design',
    '602' => 'Criminal Justice & Security',
    '603' => 'Education & Teaching',
    '604' => 'General',
    '605' => 'Health Care',
    '606' => 'IT & Computer Science',
    '607' => 'Legal',
    '608' => 'Nursing & Medical',
    '609' => 'Psychology & Social Services',
    '610' => 'Technical & Engineering',
    '611' => 'Trade & Vocational',
  );

  $form['vantage_pid'] = array(
    '#type' => 'select',
    '#title' => t('Product'),
    '#options' => $options_pid,
    '#description' => t('Please select which product or category of schools to use.'),
    '#default_value' => variable_get('vantage_pid', '601'),
    '#required' => TRUE,
  );

  $options_degree = array(
    '1' => "Associate's",
    '2' => "Bachelor's",
    '3' => "Master's",
    '4' => "Doctorate",
    '5' => "Certificate/Diploma",
  );

  $form['vantage_degree'] = array(
    '#type' => 'select',
    '#title' => t('Default Degree type'),
    '#options' => $options_degree,
    '#description' => t('Please indicate which degree type should be selected by default.'),
    '#default_value' => variable_get('vantage_degree', '2'),
    '#required' => TRUE,
  );

  $options_campus = array(
    '1' => "Online",
    '2' => "Campus",
    '3' => "Both",
  );

  $form['vantage_campus'] = array(
    '#type' => 'select',
    '#title' => t('Default Physical location'),
    '#options' => $options_campus,
    '#description' => t('Please indicate which location type should be selected by default.'),
    '#default_value' => variable_get('vantage_campus', '3'),
    '#required' => TRUE,
  );

  $form['vantage_result_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Result size'),
    '#description' => t('Specify the number of results to get back from Vantage. (Standard: 10)'),
    '#default_value' => variable_get('vantage_result_size', '10'),
    '#element_validate' => array('vantage_result_size_validate'),
    '#size' => 5,
  );

  return system_settings_form($form);
}

/**
 * Validation handler for result size field.
 */
function vantage_result_size_validate($element, &$form_state) {
  if (($element['#value'] < 1) || ($element['#value'] > 100) || ((int)$element['#value'] != $element['#value'])) {
    form_error($element, t('Result size must be an integer between 1 and 100.'));
  }
}